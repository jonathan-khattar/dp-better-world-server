package au.com.digitalpurpose.betterworld.server.service;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.repository.IdeaQueries;
import au.com.digitalpurpose.betterworld.server.repository.IdeaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IdeaService {

    private final IdeaRepository ideaRepository;

    @Autowired
    public IdeaService(IdeaRepository ideaRepository) {
        this.ideaRepository = ideaRepository;
    }

    public Iterable<Idea> findIdeas(String searchTerm, Goal goal) {
        return this.ideaRepository.findAll(IdeaQueries.createSearchQuery(searchTerm, goal));
    }

    public Idea getIdea(long id) {
        return this.ideaRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("No idea found for ID " + id));
    }

    public Idea saveIdea(Idea idea) {
        return ideaRepository.save(idea);
    }
}
